package com.hy.common.util;

import lombok.Data;

@Data
public class Result<T> {

    private T data;

    private int code;

    private String msg;

    public Result(T data) {
        this.data = data;
    }

    public Result(T data, String msg) {
        this.data = data;
        this.msg = msg;
    }

    public static <T> Result<T> ok(T data) {
        return new Result(data);
    }

    public static <T> Result<T> ok(T data, String msg) {
        return new Result<>(data,msg);
    }
}
