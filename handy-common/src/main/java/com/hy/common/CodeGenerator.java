package com.hy.common;

import org.hy.generator.config.ControllerConfig;
import org.hy.generator.config.DaoConfig;
import org.hy.generator.config.DatasourceConfig;
import org.hy.generator.config.GlobalConfig;
import org.hy.generator.config.ModelConfig;

public class CodeGenerator {
    public static void main(String[] args) throws Exception {
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setParentPackage("com.hy.user");
        globalConfig.setTrimPrefix("tb_");
        DatasourceConfig datasourceConfig = new DatasourceConfig();
        datasourceConfig.setUsername("postgres");
        datasourceConfig.setPassword("postgres");
        datasourceConfig.setUrl("jdbc:postgresql://localhost:5432/shop-admin?useUnicode=true&characterEncoding=utf-8");
        globalConfig.setDatasourceConfig(datasourceConfig);

        ModelConfig modelConfig = new ModelConfig();
        modelConfig.setModule("user");
        modelConfig.setGenGetAndSet(true);
        modelConfig.setSuperClass("com.hy.common.BaseModel");
        globalConfig.setModelConfig(modelConfig);

        DaoConfig daoConfig = new DaoConfig();
        globalConfig.setDaoConfig(daoConfig);

        ControllerConfig controllerConfig = new ControllerConfig();
        controllerConfig.setModule("user");
        globalConfig.setControllerConfig(controllerConfig);

        globalConfig.addTables("tb_role");
        globalConfig.generate();
    }
}
