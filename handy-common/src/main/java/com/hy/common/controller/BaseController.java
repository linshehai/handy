package com.hy.common.controller;

import org.apache.dubbo.config.annotation.DubboReference;

import com.hy.common.service.BaseService;
import com.hy.common.util.Result;
import lombok.extern.slf4j.Slf4j;
import org.hy.automate.base.Page;
import org.hy.automate.condition.Criteria;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.List;

@Slf4j
@RestController
public abstract class BaseController<Service extends BaseService<T>,T,ID extends Serializable> {

    @DubboReference
    private Service baseService;

    @PostMapping("/add")
    public Result<Boolean> add(@RequestBody T elem){
        if(baseService.create(elem)){
            log.info("添加成功");
            return Result.ok(true,"添加成功");
        }
        return Result.ok(false);
    }
    @PostMapping("/remove")
    public Result<Boolean> remove(ID id){
        if(baseService.delete(id)){
            return Result.ok(true,"删除成功");
        }
        return Result.ok(false);
    }
    @PostMapping("/update")
    public Result<Boolean> update(@RequestBody T elem){
        if(baseService.update(elem)){
            return Result.ok(true,"更新成功");
        }
        return Result.ok(false);
    }

    @GetMapping("/{id}")
    public Result<T> get(@PathVariable("id") ID id){
        T obj = baseService.get(id);
        if(obj!=null){
            return Result.ok(obj,"查询到记录");
        }
        return Result.ok(null);
    }

    @GetMapping("/listAll")
    public Result<List<T>> listAll(){
        List<T> list = baseService.listAll();
        if(list!=null){
            return Result.ok(list,"查询到记录");
        }
        return Result.ok(null);
    }
    @GetMapping("/list")
    public Result<List<T>> list(T elem){
        Criteria<T> criteria = getCriteria(elem);
        List<T> list = baseService.list(criteria);
        if(list!=null){
            return Result.ok(list,"查询到记录");
        }
        return Result.ok(null);
    }
    @GetMapping("/page")
    public Result<Page<T>> page(@RequestBody T elem,int pageNum,int pageSize){
        Criteria<T> criteria = getCriteria(elem);
        Page<T> listPage = baseService.listPage(criteria,pageNum,pageSize);
        if(listPage!=null){
            return Result.ok(listPage,"查询到记录");
        }
        return Result.ok(null);
    }

    protected Criteria<T> getCriteria(T elem){
        return null;
    }
}
