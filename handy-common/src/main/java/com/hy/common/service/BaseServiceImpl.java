package com.hy.common.service;

import org.apache.dubbo.config.annotation.DubboService;

import com.hy.common.model.BaseModel;
import org.hy.automate.base.BaseMapper;
import org.hy.automate.base.Page;
import org.hy.automate.base.Pageable;
import org.hy.automate.condition.Criteria;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@DubboService
public class BaseServiceImpl<Mapper extends BaseMapper<T>,T extends BaseModel> implements BaseService<T>{

    @Autowired
    private Mapper baseMapper;

    @Override
    public boolean create(T elem) {
        elem.setCreateTime(LocalDateTime.now());
        return baseMapper.insert(elem)>0;
    }

    @Override
    public boolean update(T elem) {
        elem.setUpdateTime(LocalDateTime.now());
        return baseMapper.updateByIdSelective(elem)>0;
    }

    @Override
    public boolean delete(Serializable id) {
        return baseMapper.deleteById(id)>0;
    }

    @Override
    public T get(Serializable id) {

        return baseMapper.selectById(id);
    }

    @Override
    public List<T> listAll() {
        return baseMapper.select(null);
    }

    @Override
    public Page<T> listPage(Criteria<T> criteria,int pageNum,int pageSize) {
        return baseMapper.selectPage(criteria, Pageable.of(pageNum,pageSize));
    }

    @Override
    public List<T> list(Criteria<T> criteria) {
        return baseMapper.select(criteria);
    }
}
