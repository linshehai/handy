package com.hy.common.service;

import org.hy.automate.base.Page;
import org.hy.automate.condition.Criteria;

import java.io.Serializable;
import java.util.List;

public interface BaseService<T> {


    boolean create(T elem);

    boolean update(T elem);

    boolean delete(Serializable id);

    T get(Serializable id);

    List<T> listAll();

    Page<T> listPage(Criteria<T> criteria,int pageNum,int pageSize);

    List<T> list(Criteria<T> criteria);
}
