package com.hy.common.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BaseModel {

    private Long creator;

    private LocalDateTime createTime;

    private Long updater;

    private LocalDateTime updateTime;

    private Integer status;

    private Integer delFlag;

}
