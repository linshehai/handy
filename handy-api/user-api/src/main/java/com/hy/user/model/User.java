package com.hy.user.model;

import com.hy.common.model.BaseModel;
import lombok.Data;
import org.hy.automate.annotation.Column;
import org.hy.automate.annotation.Table;

@Table("tb_user")
@Data
public class User extends BaseModel {

    @Column(name = "id",primaryKey = true)
    private Long id;

    private String name;

    private String account;

    private String pwd;

}
