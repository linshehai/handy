package com.hy.user.service;

import com.hy.common.service.BaseService;
import com.hy.user.model.User;

import java.util.List;

public interface UserService extends BaseService<User> {
    List<User> listUser();
}
