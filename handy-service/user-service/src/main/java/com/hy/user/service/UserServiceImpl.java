package com.hy.user.service;

import org.apache.dubbo.config.annotation.DubboService;

import com.hy.common.service.BaseServiceImpl;
import com.hy.user.mapper.UserMapper;
import com.hy.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@DubboService
public class UserServiceImpl extends BaseServiceImpl<UserMapper,User> implements UserService{

    @Autowired
    private UserMapper userMapper;
    @Override
    public List<User> listUser() {
        return userMapper.select(null);
    }
}
