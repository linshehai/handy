package com.hy.user.mapper;

import com.hy.user.model.User;
import org.hy.automate.base.BaseMapper;

public interface UserMapper extends BaseMapper<User> {
}
