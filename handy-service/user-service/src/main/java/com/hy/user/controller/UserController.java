package com.hy.user.controller;

import org.apache.dubbo.config.annotation.DubboReference;

import com.hy.common.controller.BaseController;
import com.hy.user.model.User;
import com.hy.user.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController<UserService,User,Long> {


}
