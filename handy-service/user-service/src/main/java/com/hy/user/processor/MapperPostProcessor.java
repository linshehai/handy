package com.hy.user.processor;

import org.hy.automate.base.BaseMapper;
import org.hy.automate.handler.GeneratorChain;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Type;

@Configuration
public class MapperPostProcessor extends AutowiredAnnotationBeanPostProcessor implements ApplicationContextAware {

    private final GeneratorChain generatorChain = new GeneratorChain();

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if(bean instanceof BaseMapper){
            SqlSessionTemplate sqlSessionTemplate = this.applicationContext.getBean(SqlSessionTemplate.class);
            org.apache.ibatis.session.Configuration configuration = sqlSessionTemplate.getConfiguration();
            Type[] modelTypes = bean.getClass().getGenericInterfaces();
            //生成sql语句
            generatorChain.doGenerate(configuration, (Class<?>) modelTypes[0]);
        }
        return super.postProcessAfterInitialization(bean, beanName);
    }

    private ApplicationContext applicationContext;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
