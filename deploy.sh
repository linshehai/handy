#!/bin/bash
imageName=$1
containerName=$2
port=$3
tag=$4
password=Harbor12345
username=jenkins
harborHost=$5
containerId=$(docker ps -a |grep -w $containerName | awk '{print $1}')
backupFile=${containerName}$(date "+%Y%m%d%H%M%S").tar
#判断容器是否在运行，是则停止并删除容器
if [ "$containerId" != "" ]; then
 echo "container is running,stopping the running container and remove it"
 docker stop $containerId >> /dev/null
 docker rm $containerId >> /dev/null
 echo "container removed"
fi

#删除镜像前先备份镜像
imageId=$(docker images | grep -w $containerName | awk '{print $1}')
if [ "$imageId" != "" ] ; then
  docker save $imageId -o /data/docker/$backupFile $imageName:$tag
  echo "removing image $imageId"
  docker rmi $imageId >> /dev/null
  echo "image removed"

fi

docker login $harborHost -u $username -p $password

echo "pulling new image"
docker pull $imageName:$tag

#如果启动失败，则回滚操作
docker run -d -p $port:$port --name $containerName $imageName:$tag
if [ $? != 0 ] ; then
  docker load < /data/docker/$backupFile
  docker run -d -p $port:$port --name $containerName $imageName:$tag
fi
